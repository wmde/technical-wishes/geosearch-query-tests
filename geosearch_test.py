#!/usr/bin/env python
# sends geosearch queries to clouldelastic test servers and records query execution times
import random
import requests
from statistics import median
from string import Template

headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'User-Agent': 'Geosearch Query Tester <wikimedia-de-tech@wikimedia.de>'
}

url = 'https://cloudelastic.wikimedia.org:8243/enwiki_content/_search'
# json with template placeholders
query_template_file = 'geosearch_query_template.json'
query_template = Template(open(query_template_file, 'r').read().replace('\n', ''))

def run_query(distance, lat, lon, size):
    query = query_template.substitute(distance=distance, lat=lat, lon=lon, size=size)

    response = requests.get(url, data=query, headers=headers, timeout=10)
    response_json = response.json()

    print('Query for lat: {} lon: {} took: {} ms with {} results using {} shards'.format(
        lat,
        lon,
        response_json['took'],
        response_json['hits']['total'],
        response_json['_shards']['total'])
    )

    return response_json['took']

def run():
    distance = '10000m'
    size = 50
    times = []

    for x in range(0, 15):
        lat = round(random.uniform(45.0, 50.0), 6)
        lon = round(random.uniform(2.0, 25.0), 6)
        time = run_query(distance, lat, lon, size)
        if time:
            times.append(time)

    print('Done. Distance: {} Size: {} Median duration: {}ms'.format(
        distance,
        size,
        str(median(times)))
    )

if __name__ == '__main__':
    run()
